package com.epam.model;

import java.io.Serializable;
import java.util.Objects;

public class Mail implements Serializable {
    private int id;
    private String senderEmail;
    private String receiverEmail;
    private String subject;
    private String body;
    private String date;

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mail)) return false;
        Mail mail = (Mail) o;
        return id == mail.id
                && senderEmail.equals(mail.senderEmail)
                && receiverEmail.equals(mail.receiverEmail)
                && subject.equals(mail.subject)
                && body.equals(mail.body)
                && date.equals(mail.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, senderEmail, receiverEmail, subject, body, date);
    }

    @Override
    public String toString() {
        return "Mail{" +
                "id='" + id + '\'' +
                "senderEmail='" + senderEmail + '\'' +
                "receiverEmail='" + receiverEmail + '\'' +
                ", subject='" + subject + '\'' +
                ", body='" + body + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}

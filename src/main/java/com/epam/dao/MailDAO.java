package com.epam.dao;

import com.epam.exception.DuplicateMailException;
import com.epam.model.Mail;
import com.epam.utils.JSONManager;

import java.io.File;
import java.util.List;
import java.util.Objects;

public class MailDAO {
    private static final String MAIL_JSON_URL = "src/main/resources/mail.json";
    private static final File JSON_FILE = new File(MAIL_JSON_URL);

    public List<Mail> findMail() {
        return JSONManager.readMail(JSON_FILE);
    }

    public Mail findMailById(int id) {
        List<Mail> mail = JSONManager.readMail(JSON_FILE);
        if (Objects.isNull(mail)) {
            return null;
        }
        return mail.stream()
                .filter(m -> m.getId() == id)
                .findFirst()
                .orElse(null);
    }

    public boolean addMail(Mail mail) throws DuplicateMailException {
        List<Mail> allMail = JSONManager.readMail(JSON_FILE);
        if (Objects.isNull(allMail)) {
            return false;
        }
        if (allMail.stream().anyMatch(m -> m.getId() == mail.getId())) {
            throw new DuplicateMailException();
        }
        allMail.add(mail);
        return JSONManager.writeMail(allMail, JSON_FILE);
    }

    public boolean deleteMail(int id) {
        List<Mail> mail = JSONManager.readMail(JSON_FILE);
        if (Objects.isNull(mail)) {
            return false;
        }
        Mail mailToDelete = mail.stream().filter(m -> m.getId() == id).findFirst().orElse(null);
        boolean removed = mail.remove(mailToDelete);
        if (removed) {
            return JSONManager.writeMail(mail, JSON_FILE);
        } else {
            return false;
        }
    }
}

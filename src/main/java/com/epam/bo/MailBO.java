package com.epam.bo;

import com.epam.dao.MailDAO;
import com.epam.exception.DuplicateMailException;
import com.epam.model.Mail;

import java.util.Objects;

public class MailBO {

    private static MailDAO mailDAO = new MailDAO();

    public boolean sendMail(Mail mail) throws DuplicateMailException {
        return mailDAO.addMail(mail);
    }

    public Mail[] getAllMails() {
        return mailDAO.findMail().toArray(new Mail[]{});
    }

    public boolean deleteMail(int id) {
        return mailDAO.deleteMail(id);
    }

    public Mail[] getMailsBySender(String senderEmail) {
        if (Objects.isNull(senderEmail) || senderEmail.isEmpty()) {
            return null;
        }
        return mailDAO.findMail().stream()
                .filter(m -> m.getSenderEmail().equals(senderEmail))
                .toArray(Mail[]::new);
    }

    public Mail[] getMailsBySubject(String subject) {
        if (Objects.isNull(subject) || subject.isEmpty()) {
            return null;
        }
        return mailDAO.findMail().stream()
                .filter(m -> m.getSubject().equals(subject))
                .toArray(Mail[]::new);
    }
}

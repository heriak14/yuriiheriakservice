package com.epam.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class DateFormatter {
    private static final String PATTERN = "E, dd MMM yyyy, HH:mm:ss";
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern(PATTERN, Locale.getDefault());

    public static String dateToString(LocalDateTime date) {
        return formatter.format(date);
    }

    public static LocalDateTime stringToDate(String s) {
        return LocalDateTime.parse(s, formatter);
    }

    public static DateTimeFormatter getFormatter() {
        return formatter;
    }
}

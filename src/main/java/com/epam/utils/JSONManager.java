package com.epam.utils;

import com.epam.model.Mail;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class JSONManager {
    private static final Logger LOG = LogManager.getLogger();

    public static List<Mail> readMail(File jsonFile) {
        LOG.info("Reading mail from json");
        if (Objects.isNull(jsonFile)) {
            LOG.error("File can't be NULL");
            return null;
        }
        String jsonContent = readJsonContent(jsonFile);
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Mail> mail = new ArrayList<>(Arrays.asList(mapper.readValue(jsonContent, Mail[].class)));
            LOG.info(mail.size() + " letters was successfully read");
            return mail;
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return null;
        }
    }

    private static String readJsonContent(File jsonFile) {
        try (InputStream stream = new FileInputStream(jsonFile)) {
            byte[] buffer = new byte[stream.available()];
            stream.read(buffer);
            return new String(buffer);
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return "";
        }
    }

    public static boolean writeMail(List<Mail> mail, File jsonFile) {
        if (Objects.isNull(jsonFile) || Objects.isNull(mail)) {
            return false;
        }
        LOG.info("Writing mail to json file");
        JsonFactory factory = new JsonFactory();
        try (JsonGenerator generator = factory.createJsonGenerator(jsonFile, JsonEncoding.UTF8)) {
            generator.writeStartArray();
            for (Mail m : mail) {
                writeMailToJson(generator, m);
            }
            generator.writeEndArray();
            LOG.info(mail.size() + " letters was successfully write to file");
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return false;
        }
        return true;
    }

    private static void writeMailToJson(JsonGenerator generator, Mail mail) throws IOException {
        generator.writeStartObject();
        generator.writeNumberField("id", mail.getId());
        generator.writeStringField("senderEmail", mail.getSenderEmail());
        generator.writeStringField("receiverEmail", mail.getReceiverEmail());
        generator.writeStringField("subject", mail.getSubject());
        generator.writeStringField("body", mail.getBody());
        generator.writeStringField("date", mail.getDate());
        generator.writeEndObject();
    }
}

package com.epam.exception;

public class DuplicateMailException extends Throwable {
    public DuplicateMailException() {
        super("Mail with duplicated id are not allowed");
    }

    public DuplicateMailException(String message) {
        super(message);
    }
}

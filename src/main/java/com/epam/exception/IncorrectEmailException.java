package com.epam.exception;

public class IncorrectEmailException extends Throwable {
    public IncorrectEmailException() {
        super("Email doesn't meet the standard");
    }

    public IncorrectEmailException(String message) {
        super(message);
    }
}

package com.epam.service.rest;

import com.epam.model.Mail;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/mail/service")
public interface MailService {

    @PUT
    @Path("/send")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json; charset=UTF-8")
    Response sendMail(Mail mail);

    @GET
    @Path("/mail")
    @Produces("application/json; charset=UTF-8")
    Response getAllMails();

    @DELETE
    @Path("/delete")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("application/json; charset=UTF-8")
    Response deleteMail(@QueryParam("id") int id);

    @GET
    @Path("/sender")
    @Produces("application/json; charset=UTF-8")
    Response getMailsBySender(@QueryParam("sender") String senderEmail);

    @GET
    @Path("/subject")
    @Produces("application/json; charset=UTF-8")
    Response getMailsBySubject(@QueryParam("subject") String subject);
}

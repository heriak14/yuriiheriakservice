package com.epam.service.rest;

import com.epam.bo.MailBO;
import com.epam.exception.DuplicateMailException;
import com.epam.model.Mail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.core.Response;
import java.util.Optional;

public class MailServiceImpl implements MailService {

    private static final Logger LOG = LogManager.getLogger();
    private MailBO mailBO;

    public MailServiceImpl() {
        mailBO = new MailBO();
    }

    @Override
    public Response sendMail(Mail mail) {
        LOG.info("Sending mail in REST service");
        Response response;
        try {
            if (mailBO.sendMail(mail)) {
                response = Response.ok().entity(mail).build();
                LOG.info("Mail was successfully sent");
            } else {
                response = Response.status(Response.Status.NOT_FOUND).build();
                LOG.error("Mail was not sent");
            }

        } catch (DuplicateMailException e) {
            LOG.error(e.getMessage());
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }

    @Override
    public Response getAllMails() {
        LOG.info("Getting all mail in REST service");
        Optional<Mail[]> mail = Optional.ofNullable(mailBO.getAllMails());
        Response response;
        if (mail.isPresent()) {
            response = Response.ok().entity(mail.get()).build();
        } else {
            LOG.error("Getting failed");
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
     }

    @Override
    public Response deleteMail(int id) {
        LOG.info("Deleting mail in REST service");
        if (mailBO.deleteMail(id)) {
            return Response.ok().build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    @Override
    public Response getMailsBySender(String senderEmail) {
        LOG.info("Getting mail by sender email in REST service");
        Optional<Mail[]> mail = Optional.ofNullable(mailBO.getMailsBySender(senderEmail));
        Response response;
        if (mail.isPresent()) {
            response = Response.ok().entity(mail.get()).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }

    @Override
    public Response getMailsBySubject(String subject) {
        LOG.info("Getting mail by subject in REST service");
        Optional<Mail[]> mail = Optional.ofNullable(mailBO.getMailsBySubject(subject));
        Response response;
        if (mail.isPresent()) {
            response = Response.ok().entity(mail.get()).build();
        } else {
            response = Response.status(Response.Status.NOT_FOUND).build();
        }
        return response;
    }
}

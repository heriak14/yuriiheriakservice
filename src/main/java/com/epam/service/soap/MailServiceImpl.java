package com.epam.service.soap;

import com.epam.bo.MailBO;
import com.epam.exception.DuplicateMailException;
import com.epam.model.Mail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.jws.WebService;

@WebService(endpointInterface = "com.epam.service.soap.MailService")
public class MailServiceImpl implements MailService {
    private static Logger logger = LogManager.getLogger();
    private MailBO mailBO;

    public MailServiceImpl() {
        mailBO = new MailBO();
    }

    @Override
    public boolean sendMail(Mail mail) {
        try {
            return mailBO.sendMail(mail);
        } catch (DuplicateMailException e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public Mail[] getAllMails() {
        return mailBO.getAllMails();
    }

    @Override
    public boolean deleteMail(int id) {
        return mailBO.deleteMail(id);
    }

    @Override
    public Mail[] getMailsBySender(String senderEmail) {
        return mailBO.getMailsBySender(senderEmail);
    }

    @Override
    public Mail[] getMailsBySubject(String subject) {
        return mailBO.getMailsBySubject(subject);
    }
}

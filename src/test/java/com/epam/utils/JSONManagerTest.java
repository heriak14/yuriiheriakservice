package com.epam.utils;

import com.epam.model.Mail;
import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.testng.Assert.*;

public class JSONManagerTest {

    private static File testJsonFile;

    @BeforeClass
    private static void addTestObject() {
        testJsonFile = new File("src/test/resources/testMail.json");
        JsonFactory factory = new JsonFactory();
        try (JsonGenerator generator = factory.createJsonGenerator(testJsonFile, JsonEncoding.UTF8)) {
            generator.writeStartArray();
            generator.writeStartObject();
            generator.writeNumberField("id", 1);
            generator.writeStringField("senderEmail", "yh@ukr.net");
            generator.writeStringField("receiverEmail", "jg@gmail.com");
            generator.writeStringField("subject", "subject");
            generator.writeStringField("body", "body");
            generator.writeStringField("date", DateFormatter.dateToString(LocalDateTime.now()));
            generator.writeEndObject();
            generator.writeEndArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testReadMail() {
        assertNull(JSONManager.readMail(new File("")));
        List<Mail> mail = JSONManager.readMail(testJsonFile);
        System.out.println(mail);
        assertNotNull(mail);
        assertEquals(mail.size(), 1);
    }

    @Test
    public void testWriteMail() {
        assertFalse(JSONManager.writeMail(null, null));
        assertFalse(JSONManager.writeMail(null, testJsonFile));
        Mail testMail = new Mail();
        testMail.setId(2);
        testMail.setSenderEmail("sender@gmail.com");
        testMail.setSenderEmail("receiver@gmail.com");
        testMail.setSubject("some subject");
        testMail.setBody("mail body");
        testMail.setDate(DateFormatter.dateToString(LocalDateTime.now()));
        assertTrue(JSONManager.writeMail(Collections.singletonList(testMail), testJsonFile));
    }

    @AfterClass
    public static void cleanTestFile() {
        JsonFactory factory = new JsonFactory();
        try (JsonGenerator generator = factory.createJsonGenerator(testJsonFile, JsonEncoding.UTF8)) {
            generator.writeStartArray();
            generator.writeEndArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}